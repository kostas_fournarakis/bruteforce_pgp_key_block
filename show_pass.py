'''
Password retrieval software for protected PGP private keys
Author: Kostas Fournarakis
'''
import os
import secrets
import sys
import time
import string
import fnmatch
import argparse
from datetime import timedelta
from timeit import default_timer as timer
from concurrent.futures import ThreadPoolExecutor
from pyunpack import Archive
import pgpy
from pgpy.constants import PubKeyAlgorithm, KeyFlags, HashAlgorithm,\
    SymmetricKeyAlgorithm, CompressionAlgorithm


NEW_LINE_CHAR = '\n'
ROCK_YOU = 'rockyou.txt'
ROCK_YOU_ARCHIVE= 'rockyou.7z'

def remove_file(filename):
    ''' If file exists, remove it '''
    if os.path.exists(filename):
        os.remove(filename)

def save_key(key, filename = '', verbosity = True):
    ''' Save private key in a file '''
    save_type = None
    file_encoding = None
    if isinstance(key, str):
        save_type = 'w'
        file_encoding = 'utf-8'
    elif isinstance(key, bytes):
        save_type = 'wb'
    elif isinstance(key, pgpy.PGPKey):
        key = str(key)
        save_type = 'w'
    else:
        print('Unknown key type')
    if save_type is not None:
        with open(filename, save_type, encoding = file_encoding) as f_writer:
            f_writer.write(key)
        if verbosity:
            print('Save: PASS')
    else:
        if verbosity:
            print('Save: FAIL')

def extract_rockyou():
    ''' Extract rockyou.txt from rockyou.7z if is not available '''
    files = os.listdir("./")
    if len([f for f in files if fnmatch.fnmatch(f, ROCK_YOU)]) == 0:
        if len([f for f in files if fnmatch.fnmatch(f, ROCK_YOU_ARCHIVE)]) == 1:
            print(f'Extracting: {ROCK_YOU}')
            Archive(ROCK_YOU_ARCHIVE).extractall("./")
        else:
            print(f'{ROCK_YOU} and {ROCK_YOU_ARCHIVE} are not available.')

class SelfTests():
    ''' Functionality for testing the main Class (ShowPass)'''
    def __init__(self):
        # Filenames
        self.test_key_filename = 'test_priv.asc'
        self.test_recipe = 'test_recipe.txt'
        self.test_wordlist = 'test_wordlist.txt'
        # Test vars
        self.test_showpass_object = ShowPass(testing = True)
        self.generated_test_key = None
        self.loaded_test_key = None
        self.random_password = None

    def test_1(self):
        ''' Armored ASCII file test '''
        print('+++ TEST: ASCII Armored keyfile')
        start = timer()
        self.generate_random_password(50)
        self.generate_private_key()
        # ASCII armored private key
        key = str(self.generated_test_key)
        save_key(key, self.test_key_filename)
        self.test_showpass()
        print("Test Time:", timer()-start)
        print('--- TEST: ASCII Armored keyfile\n')

    def test_2(self):
        ''' Binary file test '''
        print('+++ TEST: Binary keyfile')
        start = timer()
        self.generate_random_password()
        self.generate_private_key()
        # Binary key
        key = bytes(self.generated_test_key)
        save_key(key, self.test_key_filename)
        self.test_showpass()
        print("Test Time:", timer()-start)
        print('-- TEST: Binary keyfile\n')

    def test_3(self):
        ''' RockYou test '''
        print('+++ TEST: RockYou ___stresstest___')
        extract_rockyou()
        start = timer()
        with open(ROCK_YOU, 'r', errors='ignore', encoding='utf-8') as f_opener:
            lines = f_opener.readlines()
            self.random_password = lines[100].rstrip(NEW_LINE_CHAR)
        self.generate_private_key()
        # Binary key
        key = bytes(self.generated_test_key)
        save_key(key, self.test_key_filename)
        self.test_showpass(wordlist = ROCK_YOU)
        print("Test Time:", timer()-start)
        print('--- TEST: RockYou ___stresstest___\n')

    def test_4(self):
        ''' Test functions using wrong arguments '''
        print('+++ TEST: Wrong argument execution')
        start = timer()
        # Generating private key using non random password
        self.generate_private_key(']4234432eww4')
        # Saving private Key as PGPKey Object
        save_key(self.generated_test_key, self.test_key_filename)
        # Saving private Key as unknown type (int)
        save_key(len(str(self.generated_test_key)), self.test_key_filename)
        # Testing Rockyou Extraction
        extract_rockyou()
        remove_file(ROCK_YOU)
        os.rename(ROCK_YOU_ARCHIVE, ROCK_YOU_ARCHIVE+'1')
        extract_rockyou()
        os.rename(ROCK_YOU_ARCHIVE+'1', ROCK_YOU_ARCHIVE)
        # Testing Wordlist Creation
        self.create_temp_recipe(['testing','123','passNotHere'])
        # Test Bruteforce Failure since password is not in the wordlist
        self.test_showpass()
        # Loading a defected Private key
        self.generate_random_password()
        self.generate_private_key()
        # Binary key
        key = bytes(self.generated_test_key)
        save_key(key, self.test_key_filename)
        with open(self.test_key_filename, 'w', encoding='utf-8') as f_writer:
            f_writer.write("BADstr")
        self.test_showpass()
        print("Test Time:", timer()-start)
        print('-- TEST: Wrong argument execution\n')

    def generate_random_password(self, length = 30, safe = True):
        ''' Generate random alphanumeric password of given length '''
        bad_list = [NEW_LINE_CHAR,'\r']
        # Generating random password
        self.random_password = str(''.join(secrets.choice(
            string.printable) for i in range(length)).replace(NEW_LINE_CHAR, ''))
        if safe:
            for char in bad_list:
                self.random_password = self.random_password.replace(char,'')
        print("Random password: " + repr(self.random_password))

    def generate_private_key(self, password = None):
        ''' Generate private key for testing purposes '''
        internal_password = ''
        if password is None:
            internal_password = self.random_password
        else:
            internal_password = password
        self.generated_test_key = None
        # Generating primary key
        key = pgpy.PGPKey.new(PubKeyAlgorithm.RSAEncryptOrSign, 4096)
        # Adding key ID
        uid = pgpy.PGPUID.new('John Snow', comment='Legit User',
            email='john.snow@winteriscoming.gov')
        # Adding options
        key.add_uid(uid,
            usage={KeyFlags.Sign, KeyFlags.EncryptCommunications, KeyFlags.EncryptStorage},
            hashes=[HashAlgorithm.SHA256, HashAlgorithm.SHA384, HashAlgorithm.SHA512,
                HashAlgorithm.SHA224],
            ciphers=[SymmetricKeyAlgorithm.AES256, SymmetricKeyAlgorithm.AES192,
                SymmetricKeyAlgorithm.AES128],
            compression=[CompressionAlgorithm.ZLIB, CompressionAlgorithm.BZ2,
                CompressionAlgorithm.ZIP, CompressionAlgorithm.Uncompressed],
            key_expiration=timedelta(days=365))
        # self-verify the key
        assert key.verify(key)
        # Protect key using random password
        assert key.is_unlocked
        assert not key.is_public
        assert not key.is_protected
        key.protect(internal_password, SymmetricKeyAlgorithm.AES256, HashAlgorithm.SHA256)
        assert key.is_primary
        assert key.is_protected
        self.generated_test_key = key

    def test_load_private_key(self):
        ''' Test functionality for ShowPass.load_private_key() '''
        self.loaded_test_key = self.test_showpass_object.load_private_key(self.test_key_filename)
        if self.loaded_test_key is not None:
            print('Load: PASS')
        else:
            print('Load: FAIL')

    def remove_temp_files(self):
        ''' Delete all of the temporary files used for testing '''
        remove_file(self.test_key_filename)
        remove_file(self.test_recipe)
        remove_file(self.test_wordlist)

    def create_temp_recipe(self, recipe_array = None):
        ''' Creating a temporary recipe.txt file for testing purposes '''
        if recipe_array is None:
            recipe_array = ['John','Snow','wolf','1337',self.random_password]
        with open(self.test_recipe, 'w', encoding='utf-8') as output_file:
            output_file.writelines(f'{NEW_LINE_CHAR.join(f"{L}" for L in recipe_array)}')

    def test_showpass(self, wordlist = None):
        ''' Test functionality of class: ShowPass '''
        self.test_load_private_key()
        self.create_temp_recipe()
        if wordlist is None:
            self.test_showpass_object.cook_wordlist(self.test_recipe, self.test_wordlist)
            bruteforce_result = self.test_showpass_object.bruteforce(self.test_wordlist)
        else:
            bruteforce_result = self.test_showpass_object.bruteforce(wordlist)
        if bruteforce_result == self.random_password:
            print('Test Password Found:', repr(bruteforce_result))
            print('Bruteforce: PASS')
        else:
            print('Bruteforce: FAIL')
        self.remove_temp_files()

class Benchmark():
    ''' Perform bruteforce Benchmarks for various computer resources'''
    def __init__(self):
        self.wordlist = ROCK_YOU

    def cpu_benchmarks(self):
        ''' SingleCore and Multicore CPU benchmarks '''
        extract_rockyou()
        self.rock_1000('Singlecore CPU', 1)
        self.rock_1000('Multicore CPU (100)', 100)

    def rock_1000(self, benchmark_name = 'Singlecore CPU', cpu_threads = 1, length = 1000):
        ''' Benchmark against line 1000 of rockyou.txt  '''
        print(f'Benchmark: {benchmark_name} <rockyou {length}>')
        password = ''
        with open(self.wordlist, 'r', errors='ignore', encoding='utf-8') as f_opener:
            lines = f_opener.readlines()
            password= lines[length].rstrip(NEW_LINE_CHAR)
        gen_object = SelfTests()
        gen_object.generate_private_key(password = password)
        key = str(gen_object.generated_test_key)
        save_key(key = key, filename = gen_object.test_key_filename, verbosity = False)
        sp_object = ShowPass(verbosity = False, testing = True)
        sp_object.load_private_key(gen_object.test_key_filename)
        start = timer()
        sp_object.bruteforce(wordlist = self.wordlist, cpu = cpu_threads)
        print(f'Seconds: { timer()-start}\n')
        gen_object.remove_temp_files()

class ShowPass():
    ''' Main Class with Password retrieving functionality '''
    def __init__(self, verbosity = False, testing = False):
        self.testing = testing
        self.verbosity = verbosity
        self.private_key_file  = None
        self.password_found = None

    def try_password(self, password):
        ''' Try password against the private key '''
        try:
            if password[-1] == NEW_LINE_CHAR:
                password = password.rstrip(NEW_LINE_CHAR)
            if self.verbosity:
                print(repr(password))
            with self.private_key_file.unlock(password):
                self.password_found = password
                return True
        except pgpy.errors.PGPDecryptionError:
            return False

    def bruteforce(self, wordlist = 'wordlist.txt', cpu = 1, max_try = sys.maxsize):
        ''' Iterate through wordlist and call try_password for each line '''
        self.password_found = None
        if (max_try == 0) or (max_try is None):
            max_try = sys.maxsize
        with open(wordlist, 'r', encoding='utf-8', errors='ignore') as input_file:
            lines = input_file.readlines()
            cnt = 0
            if not self.testing:
                print(f'Wordlist Length: {len(lines)}\n.... ACTIVE ....')
            # CPU Multithreading
            if cpu > 1:
                for i in range(0, len(lines), cpu):
                    if cnt >= max_try:
                        return False
                    # Launch Thread Executor with max threads = cpu
                    with ThreadPoolExecutor(max_workers=cpu) as exe:
                        exe.map(self.try_password, lines[i:i+cpu])
                        cnt = i+cpu
                        # Check if password was found
                        if self.password_found is not None:
                            return self.password_found
            # CPU Singlecore
            else:
                for line in lines:
                    if cnt >= max_try:
                        return False
                    # CPU password attempt
                    if cpu == 1:
                        self.try_password(password = line)
                    else:
                        print('Incorrect CPU value!')
                        return False
                    # Check if password was found
                    if self.password_found is not None:
                        return self.password_found
                    cnt += 1
        # Return false if the wordlist was exhausted
        return False

    def cook_wordlist(self, recipe_file = 'recipe.txt', wordlist = 'wordlist.txt'):
        ''' Construct Wordlist file from a simple list text file '''
        product = []
        with open(recipe_file, 'r', encoding='utf-8') as f_opener:
            lines = f_opener.readlines()
            if not self.testing:
                print('Recipe Length:', len(lines))
            for line in lines:
                first = line
                if first[-1] == NEW_LINE_CHAR:
                    first = first[:-1]
                for i, _ in enumerate(lines):
                    second = lines[i]
                    if second[-1] == NEW_LINE_CHAR:
                        second = second[:-1]
                    product.append(first + second)
                product.append(first)
        if not self.testing:
            print('Wordlist Length:', len(product))
        with open(wordlist, 'w', encoding='utf-8') as output_file:
            output_file.writelines(f'{NEW_LINE_CHAR.join(f"{L}" for L in product)}')

    def load_private_key(self, private_key_file):
        ''' Return the protected Private key file as PGPKey object '''
        try:
            self.private_key_file,_  = pgpy.PGPKey.from_file(private_key_file)
            return self.private_key_file
        except ValueError:
            print(f'{private_key_file} is not recognized as a Private key.')
            return None


if __name__ == "__main__":
    # Arguments handling
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--secret', type=str,
        help="Password protected PGP/GPG/ASC file.")
    parser.add_argument('-w', '--wordlist', type=str,
        help="Wordlist file.")
    parser.add_argument('-c', '--cpu', type=int, default=1,
        help="Number of CPU threads. Default 1.")
    parser.add_argument('-m', '--max_try', type=int,
        help="Maximum number of wordlist lines to try.")
    parser.add_argument('-t', '--test', action='store_true',
        help="Perform core-function Self Tests")
    parser.add_argument('-b', '--benchmark', action='store_true',
        help="Perform Benchmarks against the computer resources.")
    parser.add_argument('-v', '--verbosity', action='store_true',
        help="Print each iteration.")
    parser.add_argument('-g', '--generate', action='store_true',
        help="Generate a sample file: Protected Private Key. Example: \
            --generate --gen_file 'priv_key.asc' --gen_pass 'Str0nkP4$$w8rd'")
    parser.add_argument('-gf', '--gen_file', type=str,
        help="Filename for the new key.")
    parser.add_argument('-gp', '--gen_pass', type=str,
        help="Password for the new key.")
    parser.add_argument('-cw', '--cook', type=str,
        help="Cook a wordlist. Filename of the new wordlist that will be \
            produced by a recipe text file.")
    parser.add_argument('-r', '--recipe', type=str,
        help="Recipe text file. Multiple lines of potential passwords separated by '\\n'.")

    args = parser.parse_args()
    print('--secret',repr(args.secret))
    print('--wordlist', repr(args.wordlist))
    print('--cpu', repr(args.cpu))
    print('--max_try', repr(args.max_try))
    print('--test', repr(args.test))
    print('--benchmark', repr(args.benchmark))
    print('--verbosity', repr(args.verbosity))
    print('--generate', repr(args.generate))
    print('--gen_file', repr(args.gen_file))
    print('--gen_pass', repr(args.gen_pass))
    print('--cook', repr(args.cook))
    print('--recipe', repr(args.recipe))
    print('-'*20)

    # Generate a new wordlist based on a potential password list.
    if (args.recipe and args.cook) is not None:
        s = ShowPass(verbosity=args.verbosity)
        s.cook_wordlist(args.recipe, args.cook)

    # Generate a simple key.
    if args.generate:
        if (args.gen_file or args.gen_pass) is None:
            print('Wrong arguments for key generation.')
            print("Example: --generate --gen_file 'priv_key.asc' --gen_pass 'Str0nkP4$$w8rd'")
        else:
            tests = SelfTests()
            tests.generate_private_key(args.gen_pass)
            save_key(key = tests.generated_test_key, filename = args.gen_file)

    # Perform Tests before usage.
    if args.test:
        start_total_tests_time = timer()
        tests = SelfTests()
        # Armored ASCII file test
        tests.test_1()
        # Binary file test
        tests.test_2()
        # RockYou ___stresstest___
        tests.test_3()
        # Boosting Code Coverage
        tests.test_4()
        print("Time:", timer()-start_total_tests_time)

    # Perform Benchmarks
    if args.benchmark:
        bench = Benchmark()
        bench.cpu_benchmarks()

    # Perform Bruteforce attack
    if (args.secret or args.wordlist) is not None:
        s = ShowPass(verbosity=args.verbosity)
        s.load_private_key(private_key_file=args.secret)
        start_action_time = time.time()
        result = s.bruteforce(wordlist=args.wordlist, cpu=args.cpu, max_try=args.max_try)
        if result:
            print('Password Found:', repr(result))
        end =  time.time()
        print("Execution time in seconds: ",(end-start_action_time))

    # Footer
    print("That's all folks!")
