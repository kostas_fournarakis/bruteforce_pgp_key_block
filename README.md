# Bruteforce PGP Keypair Protection

[![Pipeline](https://gitlab.com/kostas_fournarakis/bruteforce_pgp_key_block/badges/main/pipeline.svg)](https://gitlab.com/kostas_fournarakis/bruteforce_pgp_key_block/-/commits/main)
[![Pylint](https://gitlab.com/kostas_fournarakis/bruteforce_pgp_key_block/-/jobs/artifacts/main/raw/pylint/pylint.svg?job=pylint)](https://gitlab.com/kostas_fournarakis/bruteforce_pgp_key_block/-/jobs/artifacts/main/raw/pylint/pylint.log?job=pylint)
[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=kostas_fournarakis_bruteforce_pgp_key_block&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=kostas_fournarakis_bruteforce_pgp_key_block)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=kostas_fournarakis_bruteforce_pgp_key_block&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=kostas_fournarakis_bruteforce_pgp_key_block)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=kostas_fournarakis_bruteforce_pgp_key_block&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=kostas_fournarakis_bruteforce_pgp_key_block)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=kostas_fournarakis_bruteforce_pgp_key_block&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=kostas_fournarakis_bruteforce_pgp_key_block)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=kostas_fournarakis_bruteforce_pgp_key_block&metric=bugs)](https://sonarcloud.io/summary/new_code?id=kostas_fournarakis_bruteforce_pgp_key_block)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=kostas_fournarakis_bruteforce_pgp_key_block&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=kostas_fournarakis_bruteforce_pgp_key_block)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=kostas_fournarakis_bruteforce_pgp_key_block&metric=coverage)](https://sonarcloud.io/summary/new_code?id=kostas_fournarakis_bruteforce_pgp_key_block)

Wordlist attack against the password protection of a private key block.

Input:
- Key file [.asc .pgp .gpg]
- Wordlist
    - Recipe words for wordlist generation (Optional)

Output:
- Password of key file

# Dependencies

- Python 3+
- `PGPy` [URL](https://pypi.org/project/PGPy/) 
- `patool` [URL](https://pypi.org/project/patool/)
- `pyunpack` [URL](https://pypi.org/project/pyunpack/)

# Instructions

## Download Python 3
   - [https://www.python.org/downloads](https://www.python.org/downloads)

## Install packages
```bash 
pip install PGPy patool pyunpack
```

## Clone repository
```bash
git clone https://gitlab.com/kostas_fournarakis/bruteforce_pgp_key_block.git
cd bruteforce_pgp_key_block
```

## Arguments of `show_pass.py`

```powershell
usage: show_pass.py [-h] [-s SECRET] [-w WORDLIST] [-c CPU] [-m MAX_TRY] [-t] [-b] [-v] [-g] [-gf GEN_FILE] [-gp GEN_PASS] [-cw COOK] [-r RECIPE]

options:
  -h, --help            show this help message and exit
  -s SECRET, --secret SECRET
                        Password protected PGP/GPG/ASC file.
  -w WORDLIST, --wordlist WORDLIST
                        Wordlist file.
  -c CPU, --cpu CPU     Number of CPU threads. Default 1.
  -m MAX_TRY, --max_try MAX_TRY
                        Maximum number of wordlist lines to try.
  -t, --test            Perform core-function Self Tests
  -b, --benchmark       Perform Benchmarks against the computer resources.
  -v, --verbosity       Print each iteration.
  -g, --generate        Generate a sample file: Protected Private Key. Example: --generate --gen_file 'priv_key.asc' --gen_pass 'Str0nkP4$$w8rd'
  -gf GEN_FILE, --gen_file GEN_FILE
                        Filename for the new key.
  -gp GEN_PASS, --gen_pass GEN_PASS
                        Password for the new key.
  -cw COOK, --cook COOK
                        Cook a wordlist. Filename of the new wordlist that will be produced by a recipe text file.
  -r RECIPE, --recipe RECIPE
                        Recipe text file. Multiple lines of potential passwords separated by '\n'.
```


## Perform runtime self tests
To ensure that nothing is missing, a run of self tests is recommended.
```powershell
python show_pass.py --test
```
Results
```powershell
--secret None
--wordlist None
--cpu 1
--max_try None
--test True
--benchmark False
--verbosity False
--generate False
--gen_file None
--gen_pass None
--cook None
--recipe None
--------------------
+++ TEST: ASCII Armored keyfile
Random password: 'E-0@1{Z4z~lTP7cqYIMEY#KUuXFu;]0DKwjWCI  8cnZ ^9 '
Save: PASS
Load: PASS
Test Password Found: 'E-0@1{Z4z~lTP7cqYIMEY#KUuXFu;]0DKwjWCI  8cnZ ^9 '
Bruteforce: PASS
Test Time: 1.4925039999980072
--- TEST: ASCII Armored keyfile

+++ TEST: Binary keyfile
Random password: "uN\x0cniG!VHc`^'!6}_GK\x0cU5Fd5L4"
Save: PASS
Load: PASS
Test Password Found: "uN\x0cniG!VHc`^'!6}_GK\x0cU5Fd5L4"
Bruteforce: PASS
Test Time: 1.5990451999969082
-- TEST: Binary keyfile

+++ TEST: RockYou ___stresstest___
Save: PASS
Load: PASS
Test Password Found: 'alexandra'
Bruteforce: PASS
Test Time: 7.045567500001198
--- TEST: RockYou ___stresstest___

Time: 10.266129300001921
```

## Wordlist creation (Optional) 

Provide a list of words `recipe.txt`
```
John
Snow
wolf
1337
```
Cook a `wordlist.txt`
```powershell
python show_pass.py --recipe 'recipe.txt' --cook 'wordlist.txt'
```

## Perform bruteforce attack against `private_key.asc`
```powershell
python show_pass.py --secret 'private_key.asc' --wordlist 'wordlist.txt' --cpu 100
```
Results
```
.... ACTIVE ....
Password Found:  jOhnSnow1337WOLF
Execution time in seconds:  0.1361241340637207 
```

## Benchmarks
### - Manual Benchmarks
As a benchmark we generate a sample key file, protected with the password 'cassandra'. That is the password number 1000 in `rockyou.txt`.
```powershell
python show_pass.py --generate --gen_file 'cassandra.asc' --gen_pass 'cassandra'
```
On an AMD 5900X we had the following bruteforce time results:
```powershell
python show_pass.py --secret 'cassandra.asc' --wordlist 'rockyou.txt' --cpu X

Single CPU core: 
--cpu 1
Execution time in seconds:  43.80409026145935

Multiple CPU threads:
--cpu 100
Execution time in seconds:  19.673929691314697
```
### - Automated Benchmarks
```powershell
python show_pass.py --benchmark
```
```powershell
--------------------
Benchmark: Singlecore CPU <rockyou 1000>
Seconds: 43.80409026145935

Benchmark: Multicore CPU (100) <rockyou 1000>
Seconds: 19.673929691314697
```
